package selenium.wait;

public class TimeoutExceptionMessage {

    static String getCausedByMessage(Throwable lastException) {
        return (lastException != null) ? "\nCaused by: " + lastException.getClass().getName() : "";
    }
}
