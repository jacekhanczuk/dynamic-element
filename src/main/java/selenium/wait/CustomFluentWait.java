package selenium.wait;

import org.openqa.selenium.*;

public class CustomFluentWait {

    private final FluentWaitFactory waitFactory;
    private final WaitExpectedConditions waitConditions;

    public CustomFluentWait(WebDriver driver) {
        waitFactory = new FluentWaitFactory(driver);
        waitConditions = new WaitExpectedConditions();
    }

    public void clickElement(WebElement element) {
        waitFactory.getCustomWait().until(waitConditions.elementIsClicked((WrapsElement) element));
    }

    public void clear(WebElement element) {
        waitFactory.getCustomWait().until(waitConditions.elementIsCleared((WrapsElement) element));
    }

    public void submit(WebElement element) {
        waitFactory.getCustomWait().until(waitConditions.elementIsSubmitted((WrapsElement) element));
    }

    public void sendKeys(WebElement element, CharSequence... keysToSend) {
        waitFactory.getCustomWait().until(waitConditions.keysAreSent((WrapsElement) element, keysToSend));
    }

    public String getElementText(WebElement element) {
        return waitFactory.getCustomWait().until(waitConditions.elementTextIsReturned((WrapsElement) element));
    }

    public String getElementAttribute(WebElement element, String name) {
        return waitFactory.getCustomWait().until(waitConditions.elementIsDisplayed((WrapsElement) element)).getAttribute(name);
    }

    public void waitUntilInputValueIsNotEmpty(WebElement element) {
        waitFactory.getCustomWait().until(waitConditions.inputValueIsNotEmpty((WrapsElement) element));
    }

    public void waitUntilElementIsNotPresent(WebElement element) {
        waitFactory.getCustomWait().until(waitConditions.elementIsNotPresent((WrapsElement) element));
    }

    public String getTagName(WebElement element) {
        return waitFactory.getCustomWait().until(waitConditions.elementTagNameIsReturned((WrapsElement) element));
    }

    public Point getLocation(WebElement element) {
        return waitFactory.getCustomWait().until(waitConditions.elementIsDisplayed((WrapsElement) element)).getLocation();
    }

    public Dimension getSize(WebElement element) {
        return waitFactory.getCustomWait().until(waitConditions.elementIsDisplayed((WrapsElement) element)).getSize();
    }

    public Rectangle getRect(WebElement element) {
        return waitFactory.getCustomWait().until(waitConditions.elementIsDisplayed((WrapsElement) element)).getRect();
    }

    public String getCssValue(WebElement element, String propertyName) {
        return waitFactory.getCustomWait().until(waitConditions.elementIsDisplayed((WrapsElement) element)).getCssValue(propertyName);
    }

    public boolean isSelected(WebElement element) {
        return waitFactory.getCustomWait().until(waitConditions.elementIsDisplayed((WrapsElement) element)).isSelected();
    }

    public boolean isEnabled(WebElement element) {
        return waitFactory.getCustomWait().until(waitConditions.elementIsDisplayed((WrapsElement) element)).isEnabled();
    }

    public WebElement waitForElement(WebElement element) {
        waitFactory.getCustomWait().until(waitConditions.elementIsDisplayed((WrapsElement) element));
        return element;
    }
}