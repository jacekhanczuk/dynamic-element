package selenium.wait;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.WrapsElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

public class WaitExpectedConditions {

    ExpectedCondition<Boolean> elementIsClicked(WrapsElement wrapsElement) {
        return driver -> {
            wrapsElement.getWrappedElement().click();
            return true;
        };
    }

    ExpectedCondition<Boolean> elementIsCleared(WrapsElement wrapsElement) {
        return driver -> {
            wrapsElement.getWrappedElement().clear();
            return true;
        };
    }

    ExpectedCondition<Boolean> elementIsSubmitted(WrapsElement wrapsElement) {
        return driver -> {
            wrapsElement.getWrappedElement().submit();
            return true;
        };
    }

    ExpectedCondition<Boolean> keysAreSent(WrapsElement wrapsElement, CharSequence... keysToSend) {
        return driver -> {
            wrapsElement.getWrappedElement().sendKeys(keysToSend);
            return true;
        };
    }

    ExpectedCondition<String> elementTextIsReturned(WrapsElement wrapsElement) {
        return driver -> wrapsElement.getWrappedElement().getText();
    }

    ExpectedCondition<String> elementTagNameIsReturned(WrapsElement wrapsElement) {
        return driver -> wrapsElement.getWrappedElement().getTagName();
    }

    ExpectedCondition<WebElement> elementIsDisplayed(WrapsElement wrapsElement) {
        return driver -> {
            WebElement element = wrapsElement.getWrappedElement();
            return element.isDisplayed() ? element : null;
        };
    }

    ExpectedCondition<Boolean> inputValueIsNotEmpty(WrapsElement wrapsElement) {
        return driver -> !wrapsElement.getWrappedElement().getAttribute("value").isEmpty();
    }

    ExpectedCondition<Boolean> elementIsNotPresent(WrapsElement wrapsElement) {
        return driver -> {
            try {
                return !wrapsElement.getWrappedElement().isDisplayed();
            } catch (Exception ignore) {
                return true;
            }
        };
    }
}