package selenium.wait;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;

class FluentWaitFactory {

    private final WebDriver driver;

    FluentWaitFactory(WebDriver driver) {
        this.driver = driver;
    }

    @SuppressWarnings("unused")
    FluentWait<WebDriver> getCustomWait(int timeoutInSeconds) {
        return FluentWaitConfig.loadConfig(createNewInstanceOfWait(), timeoutInSeconds);
    }

    FluentWait<WebDriver> getCustomWait() {
        return FluentWaitConfig.loadConfig(createNewInstanceOfWait(), FluentWaitConfig.DEFAULT_TIMEOUT_IN_SECONDS);
    }

    private FluentWait<WebDriver> createNewInstanceOfWait() {
        return new FluentWait<WebDriver>(driver) {

            @Override
            protected RuntimeException timeoutException(String message, Throwable lastException) {
                message = message + TimeoutExceptionMessage.getCausedByMessage(lastException);
                return super.timeoutException(message, lastException);
            }
        };
    }
}