package selenium.wait;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;

class FluentWaitConfig {

    private static final int DEFAULT_POLLING_IN_MILLISECONDS = 500;
    static final int DEFAULT_TIMEOUT_IN_SECONDS = 15;

    static FluentWait<WebDriver> loadConfig(FluentWait<WebDriver> wait, int timeoutInSeconds) {
        configure(wait, timeoutInSeconds);
        return wait;
    }

    private static void setDefaultPooling(FluentWait<WebDriver> wait) {
        wait.pollingEvery(Duration.ofMillis(DEFAULT_POLLING_IN_MILLISECONDS));
    }

    private static void setTimeoutInSeconds(FluentWait<WebDriver> wait, int timeoutInSeconds) {
        wait.withTimeout(Duration.ofSeconds(timeoutInSeconds));
    }

    private static void addIgnoredExceptions(FluentWait<WebDriver> wait) {
        wait
                .ignoring(StaleElementReferenceException.class)
                .ignoring(ElementNotVisibleException.class)
                .ignoring(NoSuchElementException.class)
                .ignoring(InvalidElementStateException.class)
                .ignoring(WebDriverException.class);
    }

    private static void configure(FluentWait<WebDriver> wait, int timeoutInSeconds) {
        addIgnoredExceptions(wait);
        setTimeoutInSeconds(wait, timeoutInSeconds);
        setDefaultPooling(wait);
    }
}