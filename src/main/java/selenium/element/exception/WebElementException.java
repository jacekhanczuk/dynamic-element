package selenium.element.exception;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import selenium.element.action.WebElementAction;

public class WebElementException extends WebDriverException {

    public WebElementException(WebElementAction action, WebElement element, Exception exception, WebDriver driver) {
        super(
                action + " action failed on element: " + element.toString()
                        + " Url: " + driver.getCurrentUrl() + "\n"
                        + exception.getMessage()
        );
    }

    public WebElementException(String message, WebElement element, Exception exception, WebDriver driver) {
        super(
                message + " " + element.toString()
                        + " Url: " + driver.getCurrentUrl() + "\n"
                        + exception.getMessage()
        );
    }
}