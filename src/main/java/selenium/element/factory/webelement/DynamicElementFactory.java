package selenium.element.factory.webelement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import selenium.element.DynamicWebElement;
import selenium.element.findable.FindableElement;
import selenium.element.findable.FindableElements;

import java.util.ArrayList;
import java.util.List;

public class DynamicElementFactory implements WebElementFactory {

    private final WebDriver driver;

    public DynamicElementFactory(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public WebElement createElement(FindableElement findable, String toString) {
        return new DynamicWebElement(findable, driver) {
            @Override
            public String toString() {
                return toString;
            }
        };
    }

    @Override
    public List<WebElement> createElements(FindableElements findable, String toString) {
        List<WebElement> dynamicElements = new ArrayList<>();
        List<WebElement> foundElements = findable.find();

        for (int i = 0; i < foundElements.size(); i++) {
            final int index = i;
            dynamicElements.add(new DynamicWebElement(() -> {
                try {
                    return foundElements.get(index);
                } catch (WebDriverException ignore) {
                    return findable.find().get(index);
                }
            }, driver) {
                @Override
                public String toString() {
                    return "{" + toString + " list index[" + index + "]}";
                }
            });
        }
        return dynamicElements;
    }
}
