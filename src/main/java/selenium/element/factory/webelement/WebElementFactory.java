package selenium.element.factory.webelement;

import org.openqa.selenium.WebElement;
import selenium.element.findable.FindableElement;
import selenium.element.findable.FindableElements;

import java.util.List;

public interface WebElementFactory {
    WebElement createElement(FindableElement findable, String toString);

    List<WebElement> createElements(FindableElements findable, String toString);
}
