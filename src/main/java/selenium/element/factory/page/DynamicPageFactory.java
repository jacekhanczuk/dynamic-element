package selenium.element.factory.page;

import lombok.experimental.UtilityClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.FieldDecorator;
import selenium.element.proxy.decorator.DynamicElementFieldDecorator;

@UtilityClass
public class DynamicPageFactory {

    public static void initElements(WebDriver driver, Object page) {
        FieldDecorator decorator = new DynamicElementFieldDecorator(driver);
        PageFactory.initElements(decorator, page);
    }
}