package selenium.element.factory.subelement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public interface SubElementFactory {
    WebElement createSubElement(By by);

    List<WebElement> createSubElements(By by);
}