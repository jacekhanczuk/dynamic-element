package selenium.element.factory.subelement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WrapsElement;
import selenium.element.factory.webelement.WebElementFactory;
import selenium.element.factory.webelement.DynamicElementFactory;

import java.util.List;

public class DynamicSubElementFactory implements SubElementFactory {

    private final WebElement parent;
    private final WebElementFactory elementFactory;

    public DynamicSubElementFactory(WebElement parent, WebDriver driver) {
        this.parent = parent;
        elementFactory = new DynamicElementFactory(driver);
    }

    @Override
    public WebElement createSubElement(By by) {
        return elementFactory.createElement(() -> ((WrapsElement) parent).getWrappedElement().findElement(by),
                "{" + parent.toString() + " and under this element find element by [" + by + "]}");
    }

    @Override
    public List<WebElement> createSubElements(By by) {
        return elementFactory.createElements(() -> ((WrapsElement) parent).getWrappedElement().findElements(by),
                "{" + parent.toString() + " and under this element find elements by [" + by + "]}");
    }
}