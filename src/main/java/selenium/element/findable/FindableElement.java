package selenium.element.findable;

import org.openqa.selenium.WebElement;

@FunctionalInterface
public interface FindableElement {
    WebElement find();
}
