package selenium.element.findable;

import org.openqa.selenium.WebElement;

import java.util.List;

@FunctionalInterface
public interface FindableElements {
    List<WebElement> find();
}
