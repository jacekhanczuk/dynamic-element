package selenium.element.action;

public enum WebElementAction {
    CLICK,
    SUBMIT,
    SEND_KEYS,
    CLEAR,
    GET_TAG_NAME,
    GET_ATTRIBUTE,
    GET_LOCATION,
    GET_SIZE,
    GET_RECT,
    GET_CSS_VALUE,
    IS_SELECTED,
    IS_ENABLED,
    GET_TEXT,
    FIND_ELEMENTS,
    FIND_ELEMENT
}