package selenium.element.proxy.handler;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.Annotations;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import selenium.element.factory.webelement.DynamicElementFactory;
import selenium.element.factory.webelement.WebElementFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

public abstract class AbstractDynamicElementHandler implements InvocationHandler {

    protected final ElementLocator locator;
    protected final WebElementFactory factory;
    protected final String elementDescription;

    public AbstractDynamicElementHandler(ElementLocator locator, WebDriver driver, Field field) {
        this.locator = locator;
        factory = new DynamicElementFactory(driver);
        elementDescription = createDescriptionByField(field);
    }

    private String createDescriptionByField(Field field) {
        By by = new Annotations(field).buildBy();
        return "{locator[" + by + "], " + "field[" + field.getDeclaringClass().getSimpleName()
                + ", " + field.getType().getSimpleName() + " " + field.getName() + "]}";
    }
}