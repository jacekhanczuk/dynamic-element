package selenium.element.proxy.handler;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DynamicElementHandler extends AbstractDynamicElementHandler {

    public DynamicElementHandler(ElementLocator locator, WebDriver driver, Field field) {
        super(locator, driver, field);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            return method.invoke(
                    factory.createElement(locator::findElement, elementDescription), args);
        } catch (InvocationTargetException e) {
            throw e.getCause();
        }
    }
}