package selenium.element.proxy.decorator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WrapsElement;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import selenium.element.proxy.handler.DynamicListHandler;
import selenium.element.proxy.handler.DynamicElementHandler;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.List;

public class DynamicElementFieldDecorator extends DefaultFieldDecorator {

    private final WebDriver driver;
    private Field field;

    public DynamicElementFieldDecorator(WebDriver driver) {
        super(new DefaultElementLocatorFactory(driver));
        this.driver = driver;
    }

    @Override
    public Object decorate(ClassLoader loader, Field field) {
        this.field = field;
        return super.decorate(loader, field);
    }

    @Override
    protected WebElement proxyForLocator(ClassLoader loader, ElementLocator locator) {
        InvocationHandler handler = new DynamicElementHandler(locator, driver, field);
        return (WebElement) Proxy.newProxyInstance(
                loader, new Class[]{WebElement.class, WrapsElement.class, Locatable.class}, handler);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected List<WebElement> proxyForListLocator(ClassLoader loader, ElementLocator locator) {
        InvocationHandler handler = new DynamicListHandler(locator, driver, field);
        return (List<WebElement>) Proxy.newProxyInstance(
                loader, new Class[]{List.class}, handler);
    }
}
