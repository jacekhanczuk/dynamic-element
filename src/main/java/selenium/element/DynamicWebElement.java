package selenium.element;

import org.openqa.selenium.*;
import selenium.element.exception.WebElementException;
import selenium.element.factory.subelement.SubElementFactory;
import selenium.element.factory.subelement.DynamicSubElementFactory;
import selenium.element.findable.FindableElement;
import selenium.wait.CustomFluentWait;

import java.util.List;

import static selenium.element.action.WebElementAction.*;

public class DynamicWebElement extends WebElementWrapper {

    private final CustomFluentWait wait;
    private final SubElementFactory factory;

    public DynamicWebElement(FindableElement element, WebDriver driver) {
        super(element, driver);
        wait = new CustomFluentWait(driver);
        factory = new DynamicSubElementFactory(this, driver);
    }

    @Override
    public void click() {
        try {
            wait.clickElement(this);
        } catch (WebDriverException exception) {
            throw new WebElementException(CLICK, this, exception, driver);
        }
    }

    @Override
    public void submit() {
        try {
            wait.submit(this);
        } catch (WebDriverException exception) {
            throw new WebElementException(SUBMIT, this, exception, driver);
        }
    }


    @Override
    public void sendKeys(CharSequence... keysToSend) {
        try {
            wait.sendKeys(this, keysToSend);
        } catch (WebDriverException exception) {
            throw new WebElementException(SEND_KEYS, this, exception, driver);
        }
    }

    @Override
    public void clear() {
        try {
            wait.clear(this);
        } catch (WebDriverException exception) {
            throw new WebElementException(CLEAR, this, exception, driver);
        }
    }

    @Override
    public String getTagName() {
        try {
            return wait.getTagName(this);
        } catch (WebDriverException exception) {
            throw new WebElementException(GET_TAG_NAME, this, exception, driver);
        }
    }

    @Override
    public String getAttribute(String name) {
        try {
            return wait.getElementAttribute(this, name);
        } catch (WebDriverException exception) {
            throw new WebElementException(GET_ATTRIBUTE, this, exception, driver);
        }
    }

    @Override
    public boolean isSelected() {
        try {
            return wait.isSelected(this);
        } catch (WebDriverException exception) {
            throw new WebElementException(IS_SELECTED, this, exception, driver);
        }
    }

    @Override
    public boolean isEnabled() {
        try {
            return wait.isEnabled(this);
        } catch (WebDriverException exception) {
            throw new WebElementException(IS_ENABLED, this, exception, driver);
        }
    }

    @Override
    public String getText() {
        try {
            return wait.getElementText(this);
        } catch (WebDriverException exception) {
            throw new WebElementException(GET_TEXT, this, exception, driver);
        }
    }

    @Override
    public List<WebElement> findElements(By by) {
        try {
            return factory.createSubElements(by);
        } catch (Exception exception) {
            throw new WebElementException(FIND_ELEMENTS, this, exception, driver);
        }
    }

    @Override
    public WebElement findElement(By by) {
        try {
            return factory.createSubElement(by);
        } catch (Exception exception) {
            throw new WebElementException(FIND_ELEMENT, this, exception, driver);
        }
    }

    @Override
    public boolean isDisplayed() {
        try {
            return getWrappedElement().isDisplayed();
        } catch (WebDriverException ignore) {
            return false;
        }
    }

    @Override
    public Point getLocation() {
        try {
            return wait.getLocation(this);
        } catch (WebDriverException exception) {
            throw new WebElementException(GET_LOCATION, this, exception, driver);
        }
    }

    @Override
    public Dimension getSize() {
        try {
            return wait.getSize(this);
        } catch (WebDriverException exception) {
            throw new WebElementException(GET_SIZE, this, exception, driver);
        }
    }

    @Override
    public Rectangle getRect() {
        try {
            return wait.getRect(this);
        } catch (WebDriverException exception) {
            throw new WebElementException(GET_RECT, this, exception, driver);
        }
    }

    @Override
    public String getCssValue(String propertyName) {
        try {
            return wait.getCssValue(this, propertyName);
        } catch (WebDriverException exception) {
            throw new WebElementException(GET_CSS_VALUE, this, exception, driver);
        }
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
        return getWrappedElement().getScreenshotAs(target);
    }
}