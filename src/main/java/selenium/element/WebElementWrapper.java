package selenium.element;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WrapsElement;
import selenium.element.findable.FindableElement;

public abstract class WebElementWrapper implements WebElement, WrapsElement {

    private final FindableElement element;
    protected final WebDriver driver;

    public WebElementWrapper(FindableElement element, WebDriver driver) {
        this.element = element;
        this.driver = driver;
    }

    @Override
    public WebElement getWrappedElement() {
        return element.find();
    }
}