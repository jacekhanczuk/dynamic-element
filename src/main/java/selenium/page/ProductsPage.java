package selenium.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ProductsPage extends PageObject {

    @FindBy(xpath = "//div[@Class='BookTitle']")
    private List<WebElement> books;

    public ProductsPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getAllBooks() {
        return books;
    }
}
