package selenium.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import selenium.element.exception.WebElementException;
import selenium.element.factory.page.DynamicPageFactory;
import selenium.wait.CustomFluentWait;

public abstract class PageObject {

    private final CustomFluentWait wait;
    protected WebDriver driver;

    public PageObject(WebDriver driver) {
        DynamicPageFactory.initElements(driver, this);
        wait = new CustomFluentWait(driver);
        this.driver = driver;
    }

    public WebElement waitForElement(WebElement element) {
        try {
            return wait.waitForElement(element);
        } catch (WebDriverException exception) {
            throw new WebElementException("Wait for element failed.", element, exception, driver);
        }
    }
}