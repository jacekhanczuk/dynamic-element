package selenium.page;

import org.openqa.selenium.WebDriver;

public class PageObjectManager {

    private final WebDriver driver;
    private ProductsPage productsPage;

    public PageObjectManager(WebDriver driver) {
        this.driver = driver;
    }

    public ProductsPage getProductsPage() {
        if (productsPage == null) {
            productsPage = new ProductsPage(driver);
        }
        return productsPage;
    }
}
