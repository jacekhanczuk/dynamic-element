package selenium.driver;

import org.openqa.selenium.WebDriver;

public interface WebDriverCreator {
    WebDriver create();
}