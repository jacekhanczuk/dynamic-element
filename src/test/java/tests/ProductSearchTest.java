package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import selenium.driver.WebDriverProvider;
import selenium.page.PageObjectManager;

public class ProductSearchTest {

    private PageObjectManager manager;
    private WebDriver driver;

    @Before
    public void setUp() {
        driver = new WebDriverProvider().getDriver();
        manager = new PageObjectManager(driver);
    }

    @Test
    public void productSearchTest() {
        // PUT SOME CODE HERE
        driver.get("https://www.foyles.co.uk/architecture-design");
        manager.getProductsPage().getAllBooks().forEach(book -> System.out.println(book.getText()));
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
